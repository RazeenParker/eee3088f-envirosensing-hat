# EEE3088F Envirosensing HAT - The Elemental Board

Envirosensing HAT for use with the STM32F0Discovery Board

## Name
The Elemental Board

## Description
HAT that enables one to sense light intensity, temperature and humidity. Also contains a battery voltage sensor to monitor the battery's voltage level.\
Can be used to monitor conditions in a greenhouse or weather conditions outside.\
The battery sensor enables one to quickly know when the battery voltage is running low and needs to be recharged.

## Visuals
[Front view of PCB Board](https://media.discordapp.net/attachments/916829347569094678/978309589495853076/unknown.png?width=687&height=635)\
[Back view of PCB board](https://media.discordapp.net/attachments/916829347569094678/978309814960672768/unknown.png?width=646&height=635)

## Installation
In order for the PCB to function one will require some additional hardware components:
- 5v lithium Ion Battery (Or a 5V supply)
- USB type C
- STM32f0Discovery Board

The battery can be placed in the battery holder. The USB is used to transfer data from the Elemental Board to a pc for inspection. And the Discovery Board slots into the connector pins with the usb port of the discovery board lining up with the isb port on the Elemental board.\

The STM32f0Discovery is programmed using C. A good IDE to use is the STM32CubeIDE. It can be dowloaded from [here](https://www.st.com/en/development-tools/stm32cubeide.html#st-get-software).\
Once you've downloaded STM32CubeIDE, you can create a new project:\
New STM Project > MCU/MPU Selector > Search for STM32F051R8T6 > Select the STM32F0Discovery Board > Next > Name Project > Finish > click yes on initialise peripheral defaults. \
Then download the code from the git repo in the [Firmware directory](https://gitlab.com/RazeenParker/eee3088f-envirosensing-hat/-/tree/main/Firmware). Thereafter load the project into STM32CubeIDE. Connet the STM32F0Discovery Board to your pc and click "Build".  The code should download to the Microtroller on the STM32F0Discovery Board and you weill be ready to interface with the HAT.

## Usage
Place the STM32F0Discovery Board on the HAT with their USB ports on the same side.\
Connect the battery or power supply to the HAT.\
Place the HAT in the area where you would like to measure the light intensity, temperature and humidity. \
When you would like to view the measurements and results simply connect the USB type C from the HAT to a pc. The results will download automatically to your computer for you to view them.

## Support
If any support is needed please email the developers on one of the following emails: prkraz006@myuct.ac.za, ibribr003@myuct.ac.za, or mtlmuh008@myuct.ac.za 

## Roadmap
Future plans include inproving the Under-voltage cutoff system and improving the efficiency of the current sensor processing code.We also intend to move the exact positioing of the light sensor to somewhere near the edge on the board if future designs.

## Contributing
The project is also in relatively early stages of development, and as such has a lot of room for growth and development towards different projects. We look forward to contributions by others and are excited to see how our project may be advanced. 

If you would like to contribute, you may submit a pull request on the repository. 
In the future we would like to add additional features and alternative uses for our project. If you would like to be a part of this, please email your contributions to the relevant team member, and if you are unsure of who that may be, you may email any of them. (Emails are in the suppoert section of this ReadMe).

Kicad is used for all circuit schematics and PCB designs, and the STM32CubeIDE with C and C++ is used for all code and programming regarding with regards to the Discovery board.

## Authors and acknowledgment
Razeen Parker (PRKRAZ006), 
Muhammad Motala (MTLMUH008), 
Ibrahim Ibrahim Ahmed (IBRIBR003) 

## License
For open source projects, say how it is licensed.[Creative Commons Attribution 4.0 International license](https://choosealicense.com/licenses/cc-by-4.0/)

## Project status
Development on this project has slowed down until further notice. 
