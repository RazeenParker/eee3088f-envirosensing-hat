Contribute to the Elematal Board

When designing the Elemental Board, we had a very specific set of requirements and specifications that we aimed to meet. The project is also in relatively early stages of development, and as such has a lot of room for growth and development towards different projects. We look forward to contributions by others and are excited to see how our project may be advanced. 

If you would like to contribute, you may submit a pull request on the repository. 
In the future we would like to add additional features and alternative uses for our project. If you would like to be a part of this, please email your contributions to the relevant team member, and if you are unsure of who that may be, you may email any of them.

Likewise, if you find any problems or flaws in the project, you may raise an issue on gitlab or alert the relevant person through email. Please make sure to look at the issues that have already been raised to avoid redundancies.

Emails:

IBRIBR003@myuct.ac.za - Ibrahim, Microcontroller and software

MTLMUH008@myuct.ac.za - Muhammad, Power system

PRKRAZ006@myuct.ac.za - Razeen, Sensing system

Recommended tools

If you would like to contribute to the hardware, we recommend using Kicad for your circuit schematics and PCB designs. All of the resources provided on this repository have been made using Kicad and it would make interfacing easier for yourself and future contributors.
(You can download Kicad here: https://www.kicad.org/)

Contributions to software should be done in C and C++ as the HAT and STM32f0 chip, used in the Discovery board, support C and C++ software. While you are free to use any IDE you like, we recommend using STM32CubeIDE for programming the Discovery board. 
(You can download STM32CubeIDE here: https://www.st.com/en/development-tools/stm32cubeide.html) 
